#!/bin/bash
declare -a name=( "abr"  "abreq" "abrrand" "hmap" "liste" "listetriee" "trie" )

for i in "${name[@]}"
do
   echo "Generation du graphe "$i".pdf"
   Rscript Draw.R $i
done


