package performance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class WriteMeanFiles {
	/**
	 * calcule les moyennes dans les fichiers dans data/stockage/merge et les ecrit dans le dossier data/stockage/moy
	 */
	public static void main(String[] args) throws IOException {
		String[] solName = {
				"abr", "abreq", "abrrand", "hmap", 
				"liste", "listetriee", "trie"};
		for(String strat : solName) {
			System.out.println(strat + " : ");
			HashMap<Integer, Double> H = calculMoyenneStrat(strat);	 
			writeVals(strat, H);
		}
		System.out.println("FIN");
	}
	
	
	private static void writeVals (String strat, HashMap<Integer, Double> H) throws FileNotFoundException {
		PrintStream writer = 	new PrintStream(
								new File("data/stockage/moy/"+strat+".data"));
		List<Integer> keys = new ArrayList<Integer>(H.keySet());
		Collections.sort(keys);
		for(Integer key : keys) {
			Double moyenne = H.get(key);
			writer.println(key + " " + moyenne);
		}
		writer.close();
	}
	
	private static  HashMap<Integer, Double> calculMoyenneStrat(String strat) throws IOException {
		String fileName = "data/merge/stockage/"+strat+".data";
		BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
		
		// contient les paires (nbSolutions/temps d'execution cumule)
		HashMap<Integer, Double> valeurs  = new HashMap <Integer, Double>();
		// contient les paires (nbSolutions/nb d'apparition)
		HashMap<Integer, Integer> repetitions  = new HashMap <Integer, Integer>();

		// ***********************
		// lecture du fichier
		// ***********************
		String line = reader.readLine();
		int i = 0;
		while (line != null) {
			String[] donnees = line.split(" ");
			try{
				int nbSol = Integer.parseInt(donnees[0]);
				double temps = Double.parseDouble(donnees[1]);
				ajouterValue(nbSol, temps, valeurs, repetitions);
			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("line : " + i);
				e.printStackTrace();
			}
			// ***********************
			// On met à jour les tables de hachage
			// ***********************
			line = reader.readLine();
			
		}
		// ***********************
		// On calcule la moyenne par nb de solutions
		// ***********************
		for(Integer key : valeurs.keySet()) {
			valeurs.put(key, valeurs.get(key)/(double)repetitions.get(key));
		}
		reader.close();
		return valeurs;
	}
	
	private static void ajouterValue(int nbSol, double time, HashMap<Integer, Double> H, HashMap<Integer, Integer> repetitions) {
		Integer key = new Integer(nbSol);
		if(H.containsKey(key)) {
			H.put(key, time + H.get(key));
			repetitions.put(key, 1 + repetitions.get(key));
		} else {
			H.put(key, time);
			repetitions.put(key, 1);
		}
	}
}
