package performance;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashMap;

import main.Formule;
import main.Terme;
import utils.DNFGenerator;

public class TestListing {
	/** 
	 * genere dans data/ -> a mettre a la main dans le dossier data/listing/
	 * on test les differentes methodes de listing, on ne vas pas tester les structures de stockage
	 * listing donc etendu = false
	 * selon le nombre de variables
	 * pour chaque nbVar on effectue le même test sur plusieurs formules et mm nbVar pour calculer la moyenne
	 */
	private static void writeListing(PrintStream writer,int n, double d) {
		writer.println(n + " " + d);
	}
	
	private static int indice (String strat) {
		if (strat == "iter")
			return 0;
		if (strat == "rec")
			return 1;
		if (strat == "task")
			return 2;
		return -1;
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		double[] moyennes = new double[3];
		
		HashMap<String, Terme.listingStrategy> strat = new HashMap<String, Terme.listingStrategy>();
		strat.put("iter", Terme.listingStrategy.ITER );
		strat.put("rec", Terme.listingStrategy.REC);
		strat.put("task",  Terme.listingStrategy.TASK);
		
		
		HashMap<String, PrintStream> writer = new HashMap<String, PrintStream>();

		writer.put("iter", new PrintStream(new File("data/iter.data")));
		writer.put("rec", new PrintStream(new File("data/rec.data")));
		writer.put("task", new PrintStream(new File("data/task.data")));
		
		int iterMax = 100;
		
		int nbTermes = 1;
		int tailleMinTerme = 1;
		double probaNot = 0.5;
		double probaVar = 0.5;
		boolean aleatoire = true;
		
		double debut;
		double fin;
		for (int nbVar = 10 ; nbVar < 40 ; nbVar++ ) {
			
			moyennes[0] = 0;
			moyennes[1] = 0;
			moyennes[2] = 0;
			
			for (int iteration = 0 ; iteration < iterMax ; iteration++ ) {
				
				System.out.println("i = " + iteration + "nbVar = " + nbVar);
				
				//***********************************************
				// Initialisation
				//***********************************************
				Formule f = DNFGenerator.generateDNF(nbVar, nbTermes, tailleMinTerme, 
													probaNot, probaVar, aleatoire)
						.setListingStrategy(strat.get("iter"))
						;
				Formule f2 = f.clone()
						.setListingStrategy(strat.get("rec"))
						;
				Formule f3 = f.clone()
						.setListingStrategy(strat.get("task"))
						;
				
				//***********************************************
				// ITER
				//***********************************************
				System.out.print("ITER, ");
	
				debut = (System.currentTimeMillis()/1000.0);
				f.enleverDoublons();
				f.trouverSolutions();
				fin = (System.currentTimeMillis()/1000.0);
				moyennes[indice("iter")] += (fin - debut);
	
				//***********************************************
				// REC
				//***********************************************
				System.out.print("REC, ");
	
				debut = (System.currentTimeMillis()/1000.0);
				f2.enleverDoublons();
				f2.trouverSolutions();
				fin = (System.currentTimeMillis()/1000.0);
				moyennes[indice("rec")] += (fin - debut);				// peut etre regarder le nombre de variables libres
			
				//***********************************************
				// TASK
				//***********************************************
				System.out.println("TASK");
	
				debut = (System.currentTimeMillis()/1000.0);
				f3.enleverDoublons();
				f3.trouverSolutions();
				fin = (System.currentTimeMillis()/1000.0);
				moyennes[indice("task")] += (fin - debut);
			
				//************************************************
				
			}//fin if iter
			
			moyennes[0] /= (1000 * iterMax);
			moyennes[1] /= (1000 * iterMax);
			moyennes[2] /= (1000 * iterMax);
			
			writeListing(writer.get("iter"), nbVar, moyennes[indice("iter")]);
			writeListing(writer.get("rec"), nbVar, moyennes[indice("rec")]);
			writeListing(writer.get("task"), nbVar, moyennes[indice("task")]);

		}//fin if nbVar
		
		System.out.println("FIN");
	}

}
