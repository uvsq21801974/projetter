package performance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

public class MergeFiles {
	/**
	 * merge les fichiers de data/ et de data/stockage/ data/dans stockage/merge
	 */
	public static void main(String[] args) throws IOException {
		String[] solName = {
				"abr", "abreq", "abrrand", "hmap", 
				"liste", "listetriee", "trie"};
		for(String strat : solName) {
			System.out.println(strat + " : ");
			writeVals(strat);
		}
		System.out.println("FIN");
	}
	
	
	private static void writeVals (String strat) throws IOException {
		PrintStream writer = 	new PrintStream(
								new File("data/stockage/merge/"+strat+".data"));
		String fileName = "data/stockage/"+strat+".data";
		transfer(fileName, writer);
		fileName = "data/"+strat+".data";
		transfer(fileName, writer);
		writer.close();
	}
	
	private static void transfer(String fileName, PrintStream writer) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
		String line = reader.readLine();
		while (line != null) {
			writer.println(line);
			line = reader.readLine();
		}
		reader.close();
	}
}
