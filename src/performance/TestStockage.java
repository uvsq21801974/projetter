package performance;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;

import main.Formule;
import main.Terme;
import stockage.*;
import utils.DNFGenerator;

public class TestStockage {
	/**	
	 * genere les fichiers directement dans data/ -> a mettre a la main dans le dossier data/stockage/
	 * a chaque iteration on va generer une formule et stocker les solutions 
	 * dans une liste qui contient les doublons, ensuite on va stocker les 
	 * solutions de la liste dans les autres structures et mesurer le temps 
	 * que ça prend
	 * on peut avoir plusieurs formules avec le meme nb de solutions m alors on va
	 * apres coup faire la moyenne du temps pour chaque m qu'on a sauvegarde
	 * on ne prends pas les m < 1000 car trivial ou bien m > 50000 car prend trop de temps
	 */
	
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		ListeAvecDoublons liste = new ListeAvecDoublons();
		int iterMax = 3000;
		int nbVar = 20;
		int nbTermes = 4;
		int tailleMinTerme = 1;
		double probaNot = 0.5;
		double probaVar = 0.5;
		boolean aleatoire = true;
		
		String[] solName = {
				"abr", "abreq", "abrrand", "hmap", 
				"liste", "listetriee", "trie"};
		
		HashMap<String, ISolution> sol = new HashMap<String, ISolution>();
		
		sol.put(solName[0], new ABR());
		sol.put(solName[1], new ABREquilibre());
		sol.put(solName[2], new ABRRandomise());
		sol.put(solName[3], new HMap());
		sol.put(solName[4], new Liste());
		sol.put(solName[5], new ListeTriee());
		sol.put(solName[6], new Trie());

		HashMap<String, PrintStream> writer = new HashMap<String, PrintStream>();
		
		// initialisation des writers
		for (int i = 0 ; i < 7 ; i++) 
			writer.put(solName[i], new PrintStream(new File("data/"+ solName[i] +".data")));
		
		
		for (int iteration = 0 ; iteration < iterMax ; iteration++ ) {

			System.out.println("\t==========Iter : " + iteration + "("+nbVar+")");
			Formule f = DNFGenerator.generateDNF(nbVar, nbTermes, tailleMinTerme, 
					probaNot, probaVar, aleatoire)
					.setListingStrategy(Terme.listingStrategy.ITER)
					.setSolutionStrategy(liste);
			f.afficher();
			f.enleverDoublons();
			f.trouverSolutions();
			// On inverse l'ordre de la liste car l'ajout des solutions dans 
			// cette liste se fait en tete et
			// on veut conserver l'ordre de generation et de stockage des solutions
			// dans programme de resolution complete
			Collections.reverse(liste.listeSolutions());
			System.out.println("\t\t" + liste.nombreSolutions() + "solutions");
			// Pour chaque strat on store le temps
		loop:
			for (int i = 0 ; i < 7 ; i++) {
				if(!storeTime(solName[i], writer, liste, sol)) {
					iteration --;
					break loop;
				}
			}
			liste.reset();
		} // fin iteration
		System.out.println("Fin.");
	}
	
	//n nb solutions
	private static void writeListing(PrintStream writer, int n, double d) {
		writer.println(n + " " + d);
	}
	

	/**
	 * @param strat le nom de la structure a etudier
	 * @return 	true si le nombre de solutions est suffisant
	 * 			false sinon
	 */
	private static boolean storeTime(	
			String strat, HashMap<String, PrintStream> writer, 
			Liste liste, HashMap<String, ISolution> solutions) {
		System.out.print("\t\t\t"+strat);
		long debut;
		long fin;
		debut = System.currentTimeMillis();
		// ***************************************************************
		// on transfere l'ensemble des solutions de la liste dans la structure
		// ***************************************************************
		solutions.get(strat).stockerEnsembleSolutions(liste);
		fin = System.currentTimeMillis();
		int m =  solutions.get(strat).nombreSolutions();
		System.out.println(" " + m);
		
		// si le nombre de solutions est trop bas on ignore
		if (m > 1000 && m < 50000) {
			writeListing(writer.get(strat), m, (fin - debut)/(double)1000);
			solutions.get(strat).reset();
			return true;
		}
		//*******************************
		// on reinitialise la structure car on 
		// va la reutiliser a l'etape d'apres
		// ******************************
		solutions.get(strat).reset();
		return false;
		
	}
}

