package performance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;


public class FilterFiles {
	/**
	 * filtre les fichiers de data/stockage/merge et les ecrits dans data/stockage/filter/ -> a remettre dans merge/ à la main
	 */
	public static void main(String[] args) throws IOException {
		String[] solName = {
				"abr", "abreq", "abrrand", "hmap", 
				"liste", "listetriee", "trie"};
		for(String strat : solName) {
			System.out.println(strat + " : ");
			filterStrat(strat);
		}
		System.out.println("FIN");
	}
	
	
	private static void filterStrat (String strat) throws IOException {
		PrintStream writer = 	new PrintStream(
								new File("data/stockagefilter/"+strat+".data"));
		String fileName = "data/stockage/merge/"+strat+".data";
		filter(fileName, writer);
		writer.close();
	}
	
	private static void filter(String fileName, PrintStream writer) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
		String line = reader.readLine();
		while (line != null) {
			String[] words = line.split(" ");
			int nbSol = Integer.parseInt(words[0]);
			if (nbSol < 50000)
				writer.println(line);
			line = reader.readLine();
		}
		reader.close();
	}
}