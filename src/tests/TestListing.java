package tests;

import static org.junit.Assert.*;

import main.Formule;
import main.Terme;

import org.junit.Before;
import org.junit.Test;

import stockage.ISolution;
import stockage.Trie;
import utils.DNFGenerator;

/****************************************************************************
 * Dans cette classe on va verifier que les differentes srtategie
 * de listing renvoient bien les bons resultats pour 2 formules dont 
 * on connait deja le resultat
 ****************************************************************************/
public class TestListing {
	Formule f; Formule f2;
	ISolution sol;
	
	@Before
	public void init() {
		sol = new Trie();
		f = DNFGenerator.generateDNFDummy().setSolutionStrategy(sol);
		f2 = DNFGenerator.generateDNFDummy2().setSolutionStrategy(sol);
		
	}
	
	@Test
	public void testIter() {
		f.setListingStrategy(Terme.listingStrategy.ITER);
		f2.setListingStrategy(Terme.listingStrategy.ITER);
		
		f.trouverSolutions();
		assertEquals(3, f.solutions.nombreSolutions());
		sol.reset();
		f2.trouverSolutions();
		assertEquals(6, f2.solutions.nombreSolutions());
	}
	
	@Test
	public void testTask() {
		f.setListingStrategy(Terme.listingStrategy.TASK);
		f2.setListingStrategy(Terme.listingStrategy.TASK);
		
		f.trouverSolutions();
		assertEquals(3, f.solutions.nombreSolutions());
		sol.reset();
		f2.trouverSolutions();
		assertEquals(6, f2.solutions.nombreSolutions());
	}
	
	@Test
	public void testRec() {
		f.setListingStrategy(Terme.listingStrategy.REC);
		f2.setListingStrategy(Terme.listingStrategy.REC);
		
		f.trouverSolutions();
		assertEquals(3, f.solutions.nombreSolutions());
		sol.reset();
		f2.trouverSolutions();
		assertEquals(6, f2.solutions.nombreSolutions());
	}

}
