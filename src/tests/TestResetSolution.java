package tests;

import static org.junit.Assert.*;

import main.Formule;

import org.junit.Before;
import org.junit.Test;

import stockage.*;
import utils.DNFGenerator;

/*********************************************************************************
 * On teste ici que l'on peut bien reutiliser les structures
 * apres les avoir reinitialises
***********************************************************************************/
public class TestResetSolution {

	Formule f; Formule f2;
	
	@Before
	public void init() {
		f = DNFGenerator.generateDNFDummy();
		f2 = DNFGenerator.generateDNFDummy2();
	}
	
	@Test
	public void testABR() {
		ISolution sol = new ABR();
		f.setSolutionStrategy(sol);
		f2.setSolutionStrategy(sol);
		
		f.trouverSolutions();
		assertEquals(3, f.solutions.nombreSolutions());
		
		sol.reset();
		
		f2.trouverSolutions();
		assertEquals(6, f2.solutions.nombreSolutions());
	}
	
	@Test
	public void testABREq() {
		ISolution sol = new ABREquilibre();
		f.setSolutionStrategy(sol);
		f2.setSolutionStrategy(sol);
		
		f.trouverSolutions();
		assertEquals(3, f.solutions.nombreSolutions());
		
		sol.reset();
		
		f2.trouverSolutions();
		assertEquals(6, f2.solutions.nombreSolutions());
	}
	
	@Test
	public void testABRRand() {
		ISolution sol = new ABRRandomise();
		f.setSolutionStrategy(sol);
		f2.setSolutionStrategy(sol);
		
		f.trouverSolutions();
		assertEquals(3, f.solutions.nombreSolutions());
		
		sol.reset();
		
		f2.trouverSolutions();
		assertEquals(6, f2.solutions.nombreSolutions());
	}
	
	@Test
	public void testHMap() {
		ISolution sol = new HMap();
		f.setSolutionStrategy(sol);
		f2.setSolutionStrategy(sol);
		
		f.trouverSolutions();
		assertEquals(3, f.solutions.nombreSolutions());
		
		sol.reset();
		
		f2.trouverSolutions();
		assertEquals(6, f2.solutions.nombreSolutions());
	}
	
	@Test
	public void testListe() {
		ISolution sol = new Liste();
		f.setSolutionStrategy(sol);
		f2.setSolutionStrategy(sol);
		
		f.trouverSolutions();
		assertEquals(3, f.solutions.nombreSolutions());
		
		sol.reset();
		
		f2.trouverSolutions();
		assertEquals(6, f2.solutions.nombreSolutions());
	}
	
	@Test
	public void ListeTriee() {
		ISolution sol = new ListeTriee();
		f.setSolutionStrategy(sol);
		f2.setSolutionStrategy(sol);
		
		f.trouverSolutions();
		assertEquals(3, f.solutions.nombreSolutions());
		
		sol.reset();
		
		f2.trouverSolutions();
		assertEquals(6, f2.solutions.nombreSolutions());
	}
	@Test
	public void testTrie() {
		ISolution sol = new Trie();
		f.setSolutionStrategy(sol);
		f2.setSolutionStrategy(sol);
		
		f.trouverSolutions();
		assertEquals(3, f.solutions.nombreSolutions());
		
		sol.reset();
		
		f2.trouverSolutions();
		assertEquals(6, f2.solutions.nombreSolutions());
	}
	
	
	// **********************************************************************
	// TEST
	// **********************************************************************
	@Test
	public void testListeAvecDoublons() {
		ISolution sol = new ListeAvecDoublons();
		f.setSolutionStrategy(sol);
		f2.setSolutionStrategy(sol);
		
		f.trouverSolutions();
		assertEquals(3, f.solutions.nombreSolutions());
		
		sol.reset();
		
		f2.trouverSolutions();
		assertEquals(6, f2.solutions.nombreSolutions());
	}
}
