package tests;

import static org.junit.Assert.*;

import main.Formule;

import org.junit.Before;
import org.junit.Test;

import stockage.*;
import utils.DNFGenerator;

/****************************************************************************
 * Dans cette classe on va verifier que les differentes structures
 * contiennent bien le meme nombre de solutions
 * On va comparer deux a deux toutes les structures avec la ListeTriee
 ****************************************************************************/
public class TestSolutions {

	Formule f1;
	Formule f2;
	int nbTermes = 3;
	int tailleMinTerme = 1;
	double probaNot = 0.5;
	double probaVar = 0.5;
	boolean aleatoire = true;
	int nbVar = 5;
	
	@Before
	public void test() {
		
		f1 = DNFGenerator.generateDNF(nbVar, nbTermes, tailleMinTerme, 
				probaNot, probaVar, aleatoire)
				;
		f2 = f1.clone();
	}
	
	@Test
	public void testABR() {
		System.out.println("test abr");
		f1.setSolutionStrategy(new ListeTriee());
		f2.setSolutionStrategy(new ABR());
		f1.trouverSolutions();
		f2.trouverSolutions();
		assertTrue(f1.solutions().comparer(f2.solutions()));

	}
	
	@Test
	public void testABREquilibre() {
		System.out.println("test abreq");
		f1.setSolutionStrategy(new ListeTriee());
		f2.setSolutionStrategy(new ABREquilibre());
		f1.trouverSolutions();
		f2.trouverSolutions();
		assertTrue(f1.solutions().comparer(f2.solutions()));

	}
	
	@Test
	public void testABRRandomise() {
		System.out.println("test abrrand");
		f1.setSolutionStrategy(new ListeTriee());
		f2.setSolutionStrategy(new ABRRandomise());
		f1.trouverSolutions();
		f2.trouverSolutions();
		assertTrue(f1.solutions().comparer(f2.solutions()));

	}
	
	@Test
	public void testListesHMap() {
		System.out.println("test hmap");
		f1.setSolutionStrategy(new ListeTriee());
		f2.setSolutionStrategy(new HMap());
		f1.trouverSolutions();
		f2.trouverSolutions();
		assertTrue(f1.solutions().comparer(f2.solutions()));

	}
	
	@Test
	public void testListe() {
		System.out.println("test liste");
		f1.setSolutionStrategy(new ListeTriee());
		f2.setSolutionStrategy(new Liste());
		f1.trouverSolutions();
		f2.trouverSolutions();
		assertTrue(f1.solutions().comparer(f2.solutions()));

	}
	
	@Test
	public void testTrie() {
		System.out.println("test trie");
		f1.setSolutionStrategy(new Trie());
		f2.setSolutionStrategy(new ListeTriee());
		f1.trouverSolutions();
		f2.trouverSolutions();
		assertTrue(f1.solutions().comparer(f2.solutions()));

	}
	
	// **********************************************************************
	// teste si les doublons sont bien ajoutes dans la structure de donnees
	// **********************************************************************
	@Test
	public void testListeAvecDoublons() {

		System.out.println("test listedoublons");
		ISolution sol = new ListeAvecDoublons();
		Formule f = DNFGenerator.generateDNFDummyWithRepetition()
				.setSolutionStrategy(sol);
		
		f.trouverSolutions();
		assertEquals(4, f.solutions.nombreSolutions());
	}
}
