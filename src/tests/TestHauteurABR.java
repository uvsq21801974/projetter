package tests;

import static org.junit.Assert.*;

import main.Formule;
import main.Terme;

import org.junit.Before;
import org.junit.Test;

import stockage.ABR;
import utils.DNFGenerator;
/***************************************
 * Ici on va verifier que la hauteur de l'ABR sont corrects
 * On va verifier pour toutes les strategies de listing
*****************************************/
public class TestHauteurABR {

	Formule f; Formule f2;
	ABR abr;
	
	@Before
	public void init() {
		abr = new ABR();
		f = DNFGenerator.generateDNFDummy().setSolutionStrategy(abr);
		f2 = DNFGenerator.generateDNFDummy2().setSolutionStrategy(abr);
	}
	
	//@Test
	public void testIter() {
		f.setListingStrategy(Terme.listingStrategy.ITER);
		System.out.println(f.afficher());
		f2.setListingStrategy(Terme.listingStrategy.ITER);
		System.out.println(f2.afficher());
		f.trouverSolutions();
		assertEquals(3, abr.hauteur);
		abr.reset();
		f2.trouverSolutions();
		System.out.println(abr.hauteur);
		assertEquals(5, abr.hauteur);
	}
	
	@Test
	public void testRec() {
		f.setListingStrategy(Terme.listingStrategy.REC);
		System.out.println(f.afficher());
		f2.setListingStrategy(Terme.listingStrategy.REC);
		System.out.println(f2.afficher());
		f.trouverSolutions();
		assertEquals(3, abr.hauteur);
		abr.reset();
		f2.trouverSolutions();
		System.out.println(abr.hauteur);
		assertEquals(5, abr.hauteur);
	}
	
	@Test
	public void testTask() {
		f.setListingStrategy(Terme.listingStrategy.TASK);
		System.out.println(f.afficher());
		f2.setListingStrategy(Terme.listingStrategy.TASK);
		System.out.println(f2.afficher());
		f.trouverSolutions();
		assertEquals(3, abr.hauteur);
		abr.reset();
		f2.trouverSolutions();
		System.out.println(abr.hauteur);
		assertEquals(5, abr.hauteur);
	}
}
