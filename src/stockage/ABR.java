package stockage;

import java.util.Arrays;

import main.Solution;

//enfant droite sup enfant gauche inf
public class ABR extends AbstractSolution implements ISolution{
	NoeudABR racine;
	final static int BINARY_SIZE = 2;
	public int hauteur;
	static class NoeudABR  { 
		NoeudABR[] children = new NoeudABR[BINARY_SIZE]; 
		Solution valeur; 
		NoeudABR(Solution solution){ 
            for (int i = 0; i < BINARY_SIZE; i++) 
                children[i] = null; 
            this.valeur = solution;
        } 
    }; 
	
    public ABR() {
    	this.racine = null;
    	this.hauteur = 0;
    }
	@Override
    public String toString(){
		return "ABR";
    }
	@Override
	public void afficher() {
		if(racine != null)
			afficherRec(racine);
			//System.out.println("hauteur : " + hauteur);
		
	}

	private static void afficherRec(NoeudABR noeud) {
		if (noeud.children[0] != null) {
			afficherRec(noeud.children[0]);
		}

		noeud.valeur.afficher();
		
		if (noeud.children[1] != null) {
			afficherRec(noeud.children[1]);
		}
	}
	
	@Override
	public void ajouterSolution(Solution solution) {
		super.ajouterSolutionAff(solution);
		if(racine != null)	{
			ajouterSolutionRec(racine, solution, 2);

			//System.out.println("\t" + hauteur);
		}
		else{
			solution.table = Arrays.copyOf(solution.table, solution.table.length);
			racine = creerNoeud(solution);
			incrSolutions();
			hauteur = 1;
			//System.out.println("\t" + hauteur);
		}
			
	}
	
	protected NoeudABR creerNoeud(Solution solution) {
		return new NoeudABR(solution);
	}
	
	protected void ajouterSolutionRec(NoeudABR noeud, Solution solution, int etage) {
		int compare = solution.compareTo(noeud.valeur);
		if(compare == 1) {
			if(noeud.children[1] == null) {
				solution.table = Arrays.copyOf(solution.table, solution.table.length);
				noeud.children[1] = creerNoeud(solution);
				incrSolutions();
				if (etage > hauteur)
					hauteur = etage;
			}
			else
				ajouterSolutionRec(noeud.children[1], solution, etage + 1);
		}
		else if(compare != 0) {
			if(noeud.children[0] == null) {
				solution.table = Arrays.copyOf(solution.table, solution.table.length);
				noeud.children[0] = creerNoeud(solution);
				incrSolutions();
				if (etage > hauteur)
					hauteur = etage;
			}
			else
				ajouterSolutionRec(noeud.children[0], solution, etage + 1);
		}
	}

	@Override
	public void reset() {
		racine = null;
		nbSolutions = 0;
		this.hauteur = 0;
		
	}

	@Override
	public ListeTriee solutions() {
		ListeTriee liste = new ListeTriee();
		if(racine != null)
			return solutionsRec(liste, racine);
		else return null;
			
	}
	
	public ListeTriee solutionsRec(ListeTriee liste, NoeudABR noeud) {
		if (noeud.children[0] != null) {
			liste = solutionsRec(liste, noeud.children[0]);
		}
		
		if (noeud.children[1] != null) {
			liste = solutionsRec(liste, noeud.children[1]);
		}

		liste.ajouterSolution(noeud.valeur);
		return liste;
	}
}
