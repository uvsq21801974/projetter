package stockage;

import java.util.Arrays;
import main.Solution;

public class ABRRandomise extends ABR { 
    ABRComparateurRandomise comparator = null;
    public ABRRandomise() {
    	racine = null;
    	comparator = null;
    }
	@Override
    public String toString(){
		return "ABR randomisé";
    }

	@Override
	protected void ajouterSolutionRec(NoeudABR noeud, Solution solution, int etage) {
		if(comparator == null)
			comparator = new ABRComparateurRandomise(solution.table.length);
		int compare = comparator.compare(solution, noeud.valeur);
		if(compare == 1) {
			if(noeud.children[1] == null) {
				solution.table = Arrays.copyOf(solution.table, solution.table.length);
				noeud.children[1] = creerNoeud(solution);
				incrSolutions();
				if (etage > hauteur) 
					hauteur = etage;
			}
			else
				ajouterSolutionRec(noeud.children[1], solution, etage + 1);
		}
		else if(compare != 0) {
			if(noeud.children[0] == null) {
				solution.table = Arrays.copyOf(solution.table, solution.table.length);
				noeud.children[0] = creerNoeud(solution);
				incrSolutions();

				if (etage > hauteur)
					hauteur = etage;
			}
			else
				ajouterSolutionRec(noeud.children[0], solution, etage + 1);
		}
	}
	@Override
	public void reset() {
		racine = null;
		nbSolutions = 0;
		comparator = null;
	}
}
