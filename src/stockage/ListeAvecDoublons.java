package stockage;

import java.util.List;

import main.Solution;

// structure de donnees test pour stocker les solutions avec doublons
public class ListeAvecDoublons extends Liste {
	
	
	public ListeAvecDoublons() {
		super();
		this.nom = "listeavecdoublons";
	}
	
	public List<Solution> listeSolutions() {
		return this.solutions;
	}
	
	@Override
	public void ajouterSolution(Solution solution) {
		super.ajouter(0, solution);
		incrSolutions();
	}

}
