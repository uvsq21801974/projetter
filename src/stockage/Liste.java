package stockage;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import main.Solution;

public class Liste extends AbstractSolution  implements ISolution{
	protected String nom;
	protected List<Solution> solutions;

	public Liste() {
		solutions = new LinkedList<Solution>();
		nom = "liste";
	}
	public void afficher() {
		System.out.println("Affichage "+nom+" :");
		for(Solution sol : solutions) {
			sol.afficher();
		}
	}
	
	protected void ajouter(int pos, Solution solution) {
		super.ajouterSolutionAff(solution);
		boolean[] tab = solution.table;
		solution.table = Arrays.copyOf(tab, tab.length);
		solutions.add(pos, solution);
	}
	
	@Override
	public void ajouterSolution(Solution solution) {
		if(!solutions.contains(solution))	{
			ajouter(0, solution);
			incrSolutions();
		}
	}

	@Override
    public String toString(){
		return "Liste";
    }
	@Override
	public void reset() {
		solutions.clear();
		nbSolutions = 0;
		
	}
	
	protected Solution solution(int i) {
		return solutions.get(i);
	}
	@Override
	public ListeTriee solutions() {
		ListeTriee liste = new ListeTriee();
		for (Solution s : solutions)
			liste.ajouterSolution(s);
		return liste;
			
	}
}
