package stockage;

import main.Solution;

public abstract class AbstractSolution implements ISolution {
	protected int nbSolutions = 0;
	protected boolean bavard = false;
	protected String nom;
	
	protected void messageCreation(){
		
		System.out.println("Création de " + nom);
	}
	public ISolution setBavard(boolean b){
		bavard = b;
		return this;
	}
	public void ajouterSolutionAff(Solution solution){
		if(bavard){
			System.out.print("ajout de : ");solution.afficher();}
	}
	public int nombreSolutions(){
		return nbSolutions;
	}
	
	protected void incrSolutions() {
//		System.out.println("incr");
		nbSolutions++;
	}
	
	@Override
	public void stockerEnsembleSolutions(Liste liste) {
		for(Solution s : liste.solutions) {
			this.ajouterSolution(s);
		}
	}
}
