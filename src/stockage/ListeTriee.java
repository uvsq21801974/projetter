package stockage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import main.Solution;

public class ListeTriee extends AbstractSolution  {
	private List<Solution> solutions;

	public ListeTriee() {
		solutions = new ArrayList<Solution>();
		nom = "liste triée";
	}
	
	private void ajouter(int pos, Solution solution) {

		boolean[] tab = solution.table;
		solution.table = Arrays.copyOf(tab, tab.length);
		solutions.add(pos, solution);
	}
	
	@Override
	public void ajouterSolution(Solution solution) {
		super.ajouterSolutionAff(solution);

		
		if (this.solutions.isEmpty()) {
			ajouter(0, solution);
			incrSolutions();
			return;
		}
		
		
		if (solution.compareTo(this.solution(0)) < 0) {
			ajouter(0, solution);
			incrSolutions();
			return;
		}
		
		int last = solutions.size();
		
		if (solution.compareTo(this.solution(last - 1)) > 0) {
			ajouter(last, solution);
			incrSolutions();
			return;
		}
		
		int indice = rechercheDichotomique(solution, 0, last);
		if (indice != -1) {
			ajouter(indice, solution);
			incrSolutions();
			return;
		}
	}
	//recherche dichotomique renvoie -1 si contient deja sinon renvoie l'endroit ou faire l'ajout

		
	
	private int rechercheDichotomique(Solution solution, int debut, int last) {
		int middle = (debut + last) / 2;
		int compare = solution.compareTo(this.solution(middle));
		
		if (compare == 0) {//
			//System.out.println("contient");
			return -1;
		}
		if (debut == last) {
			//System.out.println("ne contient pas");
			return debut;
		}
		if (compare > 0) {
			//si solution supérieur a middle on regarde au dessus de middle
			return rechercheDichotomique(solution, middle + 1, last);
		} else {
		return rechercheDichotomique(solution, debut, middle);
		}
	}
	@Override
    public String toString(){
		return "Liste Triée";
    }

	@Override
	public void afficher() {
		System.out.println("Affichage "+nom+" :");
		for(Solution sol : solutions) {
			sol.afficher();
		}
	}

	@Override
	public void reset() {
		solutions.clear();
		nbSolutions = 0;
	}
	
	
	protected Solution solution(int i) {
		return solutions.get(i);
	}

	@Override
	public ListeTriee solutions() {
		ListeTriee liste = new ListeTriee();
		for (Solution s : solutions)
			liste.ajouterSolution(s);
		return liste;
			
	}
	
	public boolean comparer(ListeTriee liste) {
		System.out.println(this.nbSolutions + ", " + liste.nbSolutions);
		if (liste.nbSolutions != this.nbSolutions)
			return false;
		for (int i = 0; i < this.nbSolutions ; i++) {
			if (this.solution(i).compareTo(liste.solution(i)) != 0)
				return false;
		}
		return true;
	}
}
