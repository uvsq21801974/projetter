package stockage;

import java.util.Arrays;
import java.util.TreeSet;

import main.Solution;

public class ABREquilibre extends AbstractSolution {
	TreeSet<Solution> tree = new TreeSet<Solution>();

	@Override
	public void afficher() {
		for (Solution s:tree) {
			s.afficher();
		}
	}
	@Override
    public String toString(){
		return "ABREq";
    }

	@Override
	public void ajouterSolution(Solution solution) {
		if(!tree.contains(solution)) {
			solution.table = Arrays.copyOf(solution.table, solution.table.length);
			tree.add(solution);
			this.incrSolutions();
		}
	}

	@Override
	public void reset() {
		nbSolutions = 0;
		tree.clear();
	}
	
	@Override
	public ListeTriee solutions() {
		ListeTriee liste = new ListeTriee();
		for (Solution s : tree)
			liste.ajouterSolution(s);
		return liste;
			
	}

}
