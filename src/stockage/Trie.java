package stockage;

import main.Solution;
import utils.Helper;


public class Trie extends AbstractSolution implements ISolution{
	private static TrieNode root;
	static final int BINARY_SIZE = 2;
	private int nbVar;
	public Trie() {
		this.nbVar = -1;
		root = new TrieNode();
	}
	
	static class TrieNode  { 
        TrieNode[] children = new TrieNode[BINARY_SIZE]; 
        TrieNode(){ 
            for (int i = 0; i < BINARY_SIZE; i++) 
                children[i] = null; 
        } 
        
        public void afficherRec(boolean[] buff, int level, int max) {
    		if ( level == max ) {
    			Helper.afficherSolution(buff);
    			return;
    		}
    		for(int i = 0 ; i < BINARY_SIZE ; i++) {
    			if (children[i] != null) {
    				buff[level] = ((i!=0)?true:false);
    				children[i].afficherRec(buff, level + 1, max);
    			}
    		}
    	}
        
        public ListeTriee solutionsRec(ListeTriee liste, boolean[] buff, int level, int max) {
    		if ( level == max ) {
    			liste.ajouterSolution(new Solution(buff));
    			return liste;
    		}
    		for(int i = 0 ; i < BINARY_SIZE ; i++) {
    			if (children[i] != null) {
    				buff[level] = ((i!=0)?true:false);
    				liste = children[i].solutionsRec(liste, buff, level + 1, max);
    			}
    		}
    		return liste;
    	}
    }; 
    
    public void ajouterSolution(Solution solution) {
		super.ajouterSolutionAff(solution);
		if(nbVar == -1)
			this.nbVar = solution.table.length;
        TrieNode pCrawl = root; 
        for (int level = 0; level < nbVar; level++) { 
        	boolean index = solution.table[level];
        	int val = (index) ? 1 : 0;
            if (pCrawl.children[val] == null) {
                pCrawl.children[val] = new TrieNode(); 
                if (level == nbVar - 1) {
                	incrSolutions();
                	//System.out.println("ajout : "+nbSolutions);
                }
            }
            pCrawl = pCrawl.children[val]; 
    	}
    } 
	
	boolean search(boolean[] solution) { 
        TrieNode pCrawl = root; 
        for (int level = 0; level < this.nbVar; level++)  { 
        	boolean index = solution[level];
        	int val = (index) ? 1 : 0;
            if (pCrawl.children[val] == null) 
                return false; 
            pCrawl = pCrawl.children[val]; 
        } 
        return (pCrawl != null); 
    } 
	
	public void afficher() {
		System.out.println("Affichage trie :");

		boolean[] buff =  new boolean [nbVar];
	    for(int i = 0 ; i < BINARY_SIZE ; i++) {
	    	if (root.children[i] != null) {
	    		buff[0] = ((i!=0)?true:false);
	    		root.children[i].afficherRec(buff, 1, nbVar);
	    	}
	    }
	}
	@Override
    public String toString(){
		return "Trie";
    }@Override
	public void reset() {
    	this.nbVar = -1;
		root = new TrieNode();
		nbSolutions = 0;
		
	}

	@Override
	public ListeTriee solutions() {
		ListeTriee liste = (ListeTriee)new ListeTriee();
		boolean[] buff =  new boolean [nbVar];
		return root.solutionsRec(liste, buff, 0, nbVar);
	}
}