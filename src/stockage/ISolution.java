package stockage;

import main.Solution;


public interface ISolution {
	
	public void afficher();
	
	public void ajouterSolution(Solution solution);
	
	public int nombreSolutions();
	
	public String toString();
	
	public void reset();
	
	public ISolution setBavard(boolean b);
	
	// ensemble des solutions sous la forme d'une liste triee
	// pour les test junits
	public ListeTriee solutions();
	
	// on stocke l'ensemble des solutions de la liste dans la structure
	// pour generer des graphes
	public void stockerEnsembleSolutions(Liste liste);
}
