package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TableauxVariables {
	public List<Variable> positifs = new ArrayList<Variable>();
	public List<Variable> negatifs = new ArrayList<Variable>();
	private int negN = 0;
	private int posN = 0;
	private static final String not = "¬";
	public boolean contient(Variable v) {
		for (int i = 0; i < positifs.size(); i++) {
			if (positifs.get(i) == v)
				return true;
			if (negatifs.get(i) == v)
				return true;
		}
		return false;
	}
	public TableauxVariables clone(){
		TableauxVariables t = new TableauxVariables();
		t.positifs.addAll(this.positifs);
		t.negatifs.addAll(this.negatifs);
		t.negN = this.negN;

		t.posN = this.posN;
		return t;
	}
	public void ajouterVariable(Variable v, boolean signe) {

		if (contient(v))
			return;
		if (signe) {
			posN++;
			positifs.add(v);
			negatifs.add(null);
		} else {
			negN++;
			positifs.add(null);
			negatifs.add(v);
		}
	}
	//les var qui apparaissent uniquement

	public List<Variable> variables() {
		List<Variable> variables = new ArrayList<>();
		for (int i = 0; i < positifs.size(); i++) {
			Variable v;
			if (positifs.get(i) == null) {
				v = negatifs.get(i);
			} else {
				v = positifs.get(i);
			}
			variables.add(v);
		}
		return variables;
	}

	public void trier(){
		if (negN == 0) {
			Collections.sort(positifs);
			return;
		}
		if (posN == 0) {
			Collections.sort(negatifs);
			return;
		}
		while(positifs.remove(null)){}
		while(negatifs.remove(null)){}
		Collections.sort(positifs);
		Collections.sort(negatifs);
		int lastp = posN -1;
		int lastn = negN -1;
		Variable[] tmpPos = new Variable[posN + negN];
		Variable[] tmpNeg = new Variable[posN + negN];
		int i = 0;
		for(Variable v : positifs)
			tmpPos[i++] = v;
		i = 0;
		for(Variable v : negatifs)
			tmpNeg[i++] = v;
		for (i = negN + posN - 1 ; i != -1 ; i--) {
			Variable vp = positifs.get(lastp);
			Variable vn = negatifs.get(lastn);
			if (vp.compareTo(vn) > 0) {
				tmpPos[i] = vp;
				tmpPos[lastp] = null;
				lastp--;
				if (lastp < 0)
					break;
			} else {
				tmpNeg[i] = vn;
				tmpNeg[lastn] = null;
				lastn--;
				if (lastn < 0)
					break;
			}

		}
		this.positifs = Arrays.asList(tmpPos);
		this.negatifs = Arrays.asList(tmpNeg);
	}
	public int taille() {
		return positifs.size();
	}
	public String toString(){
		String res = "";
		for (int i = 0; i < positifs.size(); i++) 
			if (positifs.get(i) == null) 
				res += not + negatifs.get(i).toString();
			else
				res += positifs.get(i).toString();
		return res;
	}
	

}
