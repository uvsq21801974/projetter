package main;

/**
 * Liste chainee : on ne voulait pas utiliser une linkedList car on n'a pas acces directement au pointeur
 *
 */
public class Link {
	public Variable val;
	public Link next;
	public Link before;
	public Link last;
	
	public Link(Variable val, Link next) {
		this.val= val;
		this.next= next;
		this.before = null;
		if (next == null) 
			this.last = this;
		else {
			this.last = next.last;
			next.before = this;
		}
	}
	
	public String toString() {
		String s= "[ "+val;
		for (Link cur=next; cur!=null; cur= cur.next) {
		    s= s+" "+ cur.val;
		}
		s= s+" ]";
		return s;
	}
	
	public boolean contient(Variable v){
		Link cur = this;
		while(cur != null && cur.val == v){
			cur = cur.next;
		}
		return cur != null;
	}
	


}