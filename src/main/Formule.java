package main;
import java.util.ArrayList;
import java.util.List;


import stockage.ISolution;
import stockage.ListeTriee;



public class Formule {
	
	private static final String and = " v ";
	public int nbVar;

	public List<Variable> variables;
	public List<Terme> termes;
	public ISolution solutions;
	
	private boolean etendue;
	public boolean triee;
	private Terme.listingStrategy listingStrategy = Terme.listingStrategy.ITER;
	public Formule(int n) {
		nbVar = n;
		termes = new ArrayList<Terme>();
		etendue = false;
		triee = false;
		
	}
	
	public Formule setSolutionStrategy(ISolution sol){
		this.solutions = sol;
		return this;
	}
	public Formule setListingStrategy(Terme.listingStrategy strat){
		this.listingStrategy = strat;
		return this;
	}
	public void trouverSolutions() {
		if(!etendue) {
			System.out.println("Solution pour formule non étendue");
			for(Terme t:this.termes){
				t.trouverSolutions(solutions, this.variables, listingStrategy);
			}
		} else {
			System.out.println("Solution pour formule étendue");
			for(Terme t:this.termes){
				solutions.ajouterSolution(new Solution(t.solutionFixe(this.variables.size())));
			}
		}
	}
	
	public void ajouterTerme(Terme terme) {
		termes.add(terme);
	}
	
	
	public String afficher() {
		String res = "   ";
		for(Terme t : termes) {
			res+= t.toString() + and;
		}
		return res.substring(0, res.length() - and.length());
	}
	
	public void etendre() {
		List<Terme> nvTermes = new ArrayList<Terme>();
		for (Terme t : termes) {
			List<Variable> vars = new ArrayList<Variable>();
			vars.addAll(this.variables);
			vars.removeAll(t.variables());
			nvTermes.addAll(t.etendre(vars));
		}
		etendue = true;
		termes = nvTermes;
	}
	
	public void enleverDoublons() {
		List<Terme> nvTermes = new ArrayList<Terme>();
		if(triee) {
			System.out.println("formule deja triee");
			for (Terme t : termes) {
//				System.out.print("Analyse du terme  "+t);
				if(!t.estDansListe(nvTermes)) {
					//System.out.println(" -> "+t);
					nvTermes.add(t);
				}
				//else {System.out.println(" : suppression doublon ");}
			}
			termes = nvTermes;
			return;
		}
		for (Terme t : termes) {
//			System.out.print("Analyse du terme  "+t);
			t.trierTerme();
			if(!t.estDansListe(nvTermes)) {
//				System.out.println(" -> "+t);
				nvTermes.add(t);
			}
			//else {System.out.println(" : suppression doublon ");}
		}
		termes = nvTermes;
	}
	
	public Formule clone() {
		Formule newF = new Formule(nbVar);
		newF.termes = new ArrayList<Terme>();
		for(Terme t: this.termes) {
			newF.termes.add(t.clone());
		}
		newF.triee = this.triee;
		newF.variables = new ArrayList<Variable>(this.variables);
		return newF;
	}
	
	public ListeTriee solutions() {
		return this.solutions.solutions();
	}
}
