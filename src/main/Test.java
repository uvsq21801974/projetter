package main;

import stockage.*;
import utils.DNFGenerator;
import utils.Helper;

public class Test {

	public static void main(String [] args) {
		//si aleatoire = true alors les variables apparaitront dans un ordre aleatoire sinon dans l'ordre
		boolean aleatoire = true;
		//nb de variables differentes dans la formule
		int nbVar = 3;
		//nb de termes maximum dans la formule
		int nbMaxTermes = 2;
		//taille minimum du terme ( en variables )
		int tailleMinTerme = 1;
		//proba d'aparition qu"un variable soit negative
		double probaNot = 0.3;
		//proba d'aparition qu"une variable apparraisse dans chaque terme
		double probaVar = 0.5;
		Terme.listingStrategy strat;// = Terme.listingStrategy.ITER1;
		strat = Terme.listingStrategy.ITER;
		//strat = Terme.listingStrategy.REC;
		//strat = Terme.listingStrategy.TASK;
		
		//structure qu'on va utiliser pour stocker la solution : soit trie, soit liste
		ISolution sol;
		sol= new Trie();
//		sol = new Liste();
//		sol = new ListeTriee();
//		sol= new ABR();
//		sol = new ABREquilibre();
//		sol = new ABRRandomise();
		//sol = new HMap();
		//sol.setBavard(true);
		boolean extension = false;
		Formule formule = DNFGenerator.generateDNF(nbVar, nbMaxTermes, tailleMinTerme, probaNot, probaVar, aleatoire)
				.setSolutionStrategy(sol)
				.setListingStrategy(strat);
		Helper.afficherListeVar(formule.variables);
		System.out.println("Formule : "+formule.afficher());
		if(extension)
			formule.etendre();
		
		System.out.println("formule etendue : "+formule.afficher());
		
		formule.enleverDoublons();
		System.out.println("Formule classee : "+formule.afficher());
		

		formule.trouverSolutions();
		
		
		if(formule.solutions != null) {
			formule.solutions.afficher();
			System.out.println(formule.solutions.nombreSolutions()+ " solutions\nfin.");
		}
		System.out.println("Fin de l'éxecution");
	}
}
