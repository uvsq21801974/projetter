package main;

public class Variable implements Comparable<Variable>{
		public final int id;
		public static int cpt = 0;
		public Variable() {
			this.id = cpt;
			cpt++;
		}
		@Override
		public int compareTo(Variable other) {
			//on met les null en fin de liste
			if (this == null) {
				return 1;
			}
			if (other == null) {
				return -1;
			}
			if (this.id > other.id)
				return 1;
			if (this.id < other.id)
				return -1;
			return 0;
		}
		public String toString() {
			return "X"+id;
		}
	}

