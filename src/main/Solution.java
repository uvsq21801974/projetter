package main;

import utils.Helper;


public class Solution implements Comparable<Solution>{
	public boolean[] table;
	
	public Solution(boolean[] sol) {
		// table n'est qu'une REFERENce à l'initialisation il faudra faire une copie a lajout 
		// (dans les implementations de ISolution) 
		table = sol;
	}
	public void afficher() {
		String s = "";
		for(int i = 0; i<table.length;i++) {
			s+=((table[i])?1:0);
		}
		System.out.println(s + ((table.length > 32)?"":" : " + Helper.binaireAEntier(table)));
	}
	
	@Override
	public int hashCode(){
		int i = 0;
		int nbVar = table.length;
		//31 et pas 32 car un bit de signe
		int jump = 31;
		int cum = 0;
		int t;
		while(i + jump <  nbVar) {
			t = Helper.binaireAEntier(table, i, i + jump);
			cum = cum ^ t;
			i += jump;
		}
		if(i + jump >= nbVar ){
			t = Helper.binaireAEntier(table, i, table.length);
			cum = cum ^ t;
		}
		return cum;
		
	}
	
	@Override
	public int compareTo(Solution arg0) {
		int i =  0;
		//le bit le plus fort est table[0]
		while (i != this.table.length) {
			if (this.table[i] && !arg0.table[i])
				return 1;
			if (!this.table[i] && arg0.table[i])
				return -1;
			i++;
		}
		return 0;
	}
	
	@Override
	public boolean  equals(Object other){
		if (! (other instanceof Solution))
			return false;
		return (this.compareTo(((Solution)other)) == 0);
	}
}
