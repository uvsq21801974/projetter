package utils;
import java.util.Arrays;
import java.util.Collections;

import main.Formule;
import main.Terme;
import main.Variable;

public class DNFGenerator {
	/**
	 * aleatoire = false : genere Xi dans l'ordre croissant des Xi
	 * sinon dans un ordre aleatoire
	 * probanot probabilité qu'une variable soit negative
	 * probava probabilité d'apparition d'une variable
	 */
	public static Formule generateDNF(int nbVar, int nbTermes, int tailleMinTerme, 
			double probaNot, double probaVar, boolean aleatoire) {

		Formule formule = new Formule(nbVar);
		VariableFactory vf = new VariableFactory(nbVar, aleatoire);
		int i = 0;
		while(i++ < 9000 && formule.termes.size() != nbTermes) {
			Terme terme = new Terme();
			for (int j = 0 ; j < nbVar ; j++){
				Variable var = vf.getVariable();
				//variables.put(var, null);
				if (Math.random() < probaVar)
					terme.ajouterVariable(var, Math.random() < 1 - probaNot);
			}
			if(terme.taille() >= tailleMinTerme)
				formule.ajouterTerme(terme);

			vf.reset();
		}
		formule.variables = Arrays.asList(vf.variables);
		Collections.sort(formule.variables);
		formule.triee = !aleatoire;
		
		//On remet à 0 le compteur pour des generations de formules ulterieures
		Variable.cpt = 0;
		return formule;
	}
	
	// **************************************************************************
	// Generation de formules connues pour les tests
	// **************************************************************************
	public static Formule generateDNFDummy() {
		int nbVar = 3;
		Formule formule = new Formule(nbVar);
		VariableFactory vf = new VariableFactory(nbVar, false);
		
		Terme terme = new Terme();
		terme.ajouterVariable(vf.getVariable(2),  false);
		terme.ajouterVariable(vf.getVariable(0),  true);
		formule.ajouterTerme(terme);
		vf.reset();
		
		terme = new Terme();
		terme.ajouterVariable(vf.getVariable(2),  true);
		terme.ajouterVariable(vf.getVariable(1),  true);
		terme.ajouterVariable(vf.getVariable(0),  true);
		formule.ajouterTerme(terme);

		formule.variables = Arrays.asList(vf.variables);
		Collections.sort(formule.variables);
		formule.triee = false;
		Variable.cpt = 0;

		return formule;
	}
	
	public static Formule generateDNFDummy2() {
		int nbVar = 4;
		Formule formule = new Formule(nbVar);
		VariableFactory vf = new VariableFactory(nbVar, false);
		
		Terme terme = new Terme();
		terme.ajouterVariable(vf.getVariable(2),  false);
		terme.ajouterVariable(vf.getVariable(0),  true);
		formule.ajouterTerme(terme);
		vf.reset();
		
		terme = new Terme();
		terme.ajouterVariable(vf.getVariable(2),  true);
		terme.ajouterVariable(vf.getVariable(1),  true);
		terme.ajouterVariable(vf.getVariable(0),  true);
		formule.ajouterTerme(terme);

		formule.variables = Arrays.asList(vf.variables);
		Collections.sort(formule.variables);
		formule.triee = false;
		Variable.cpt = 0;

		return formule;
	}
	
	// *************************************************
	// generation d'une formule avec une solution qui 
	// reapparait entre ses termes
	// n(X1)X2 and X0X2
	// solutions :
	// 101, 001
	// 101, 111
	// -> 101 reapparait
	// *************************************************
	public static Formule generateDNFDummyWithRepetition() {
		int nbVar = 3;
		Formule formule = new Formule(nbVar);
		VariableFactory vf = new VariableFactory(nbVar, false);
		
		Terme terme = new Terme();
		terme.ajouterVariable(vf.getVariable(2),  true);
		terme.ajouterVariable(vf.getVariable(1),  false);
		formule.ajouterTerme(terme);
		vf.reset();
		
		terme = new Terme();
		terme.ajouterVariable(vf.getVariable(2),  true);
		terme.ajouterVariable(vf.getVariable(0),  true);
		formule.ajouterTerme(terme);

		formule.variables = Arrays.asList(vf.variables);
		Collections.sort(formule.variables);
		formule.triee = false;
		Variable.cpt = 0;

		return formule;
	}

}

