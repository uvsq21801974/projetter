package utils;
import java.util.List;
import java.util.Random;

import main.Variable;


public class Helper {
	
	public static void afficherListeVar(List<Variable> var) {
		for(Variable v : var) {
			System.out.print(v+ " ");
		}
		System.out.println();
	}
	
	public static void afficherSolution(boolean[] solution) {
		String s = "";
		for(int i = 0; i<solution.length;i++) {
			s+=((solution[i])?1:0);
		}
		System.out.println(s );//+ " : " + binaireAEntier(solution));
	}
	public static String booleenAString(boolean[] solution) {
		String binaire = "";
		for (int i = 0 ; i < solution.length ; i++) {
			if(solution[i])
				binaire+="1";
			else
				binaire+="0";
		}
		return binaire;
	}
	
	static public int binaireAEntier(boolean[] solution, int debut, int fin) {
		String s = booleenAString(solution, debut, fin);
		//System.out.println(s);
		//System.out.println("size :"+solution.length);
		return Integer.parseInt(s, 2);
	}
	
	public static String booleenAString(boolean[] solution, int debut, int fin) {
		String binaire = "";
		for (int i = debut ; i < fin ; i++) {
			if(solution[i])
				binaire+="1";
			else
				binaire+="0";
		}
		return binaire;
	}
	
	static public int binaireAEntier(boolean[] solution) {
		String s = booleenAString(solution);
		//System.out.println(s);
		//System.out.println("size :"+solution.length);
		return Integer.parseInt(s, 2);
	}
	
	static public int comparerSolutions(boolean[] s1, boolean[] s2) {
		//si s1 > s2 -> 1 si s2 < s1 -> -1 sinon 0
		for (int i = s1.length - 1 ; i >= 0 ; i--) {
			if(s1[i] && !s2[i])
				return 1;
			if(s2[i] && !s1[i])
				return -1;
		}
		return 0;
	}
	public static void shuffleArray(int[] array) {
	    int index;
	    Random random = new Random();
	    for (int i = array.length - 1; i > 0; i--) {
	        index = random.nextInt(i + 1);
	        if (index != i) {
	            array[index] ^= array[i];
	            array[i] ^= array[index];
	            array[index] ^= array[i];
	        }
	    }
	}
	public static void shuffleArray(boolean[] array) {
	    int index;
	    Random random = new Random();
	    for (int i = array.length - 1; i > 0; i--) {
	        index = random.nextInt(i + 1);
	        if (index != i) {
	            array[index] ^= array[i];
	            array[i] ^= array[index];
	            array[index] ^= array[i];
	        }
	    }
	}
}
